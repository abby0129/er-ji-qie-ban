$(function(){
	// Init
	// 耳機材質
	const headsetMaterialTitleArray = Object.values(headsetMaterialList).map(item => item.materialTitle);
	const headsetMaterialCodeArray = Object.values(headsetMaterialList).map(item => item.materialCode);
	
	// 耳機顏色 
	const headsetColorTitleArray = Object.values(headsetList).map(item => item.colorTitle);
	const headsetColorCodeArray = Object.values(headsetList).map(item => item.colorCode);
	
	// 接頭
	const headsetconnectorTitleArray = Object.values(headsetconnectorList).map(item => item.connectorTitle);
	const headsetconnectorCodeArray = Object.values(headsetconnectorList).map(item => item.connectorCode);

	// 電纜
	const headsetcableTitleArray = Object.values(headsetcableList).map(item => item.cableTitle);
	const headsetcableCodeArray = Object.values(headsetcableList).map(item => item.cableCode);

	let productTitle = $('.productTitle').text();
	let headsetMaterial = '';
	let headsetFullEarColor = '';
	let headsetRightEarColor = '';
	let headsetLeftEarColor = '';
	let nouelogo = false;
	let orderprice = 0;
	let orderdata = {};
	// 上傳你的藝術作品
	let custompicture = '否';
	// 上傳你的藝術作品-向右鏡像
	let custommirrorright = '否';

	
	$('.headset__color-group.rightear').hide();
	$('.headset__color-group.leftear').hide();
	orderPriceHandle();

	// 動態產出耳機材質選項
	$.each(headsetMaterialCodeArray, function(index, value) {
		$('.headsetmaterial__category').append(
		'<li>' +
		'	<div class="form__group-radio">' +
		'		<input type="radio" name="headset__category" value="' + value + '" id="' + value + '">' +
		'		<label for="' + value + '">' + headsetMaterialTitleArray[index] + '</label>' +
		'	</div>' +
		'</li>'
		)
	});

	// fullear 動態產出顏色選項
	$.each(headsetColorTitleArray, function(index, value) {
		$('.headset__color-group.fullear .headset__color-items').append(
			'<div class="form__group-radio">' +
			'	<label for="fullear-' + value + '">' + 
			'		<input type="radio" name="headset__color-fullear" value="fullear-' + value + '" id="fullear-' + value + '">' +
			'		<span class="checkmark ' + value + '"></span>' +
			'	</label>' +
			'</div>'
		)
	});

	// leftear 動態產出顏色選項
	$.each(headsetColorTitleArray, function(index, value) {
		$('.headset__color-group.leftear .headset__color-items').append(
			'<div class="form__group-radio">' +
			'	<label for="leftear-' + value + '">' + 
			'		<input type="radio" name="headset__color-leftear" value="leftear-' + value + '" id="leftear-' + value + '">' +
			'		<span class="checkmark ' + value + '"></span>' +
			'	</label>' +
			'</div>'
		)
	});

	// rightear 動態產出顏色選項
	$.each(headsetColorTitleArray, function(index, value) {
		$('.headset__color-group.rightear .headset__color-items').append(
			'<div class="form__group-radio">' +
			'	<label for="rightear-' + value + '">' + 
			'		<input type="radio" name="headset__color-rightear" value="rightear-' + value + '" id="rightear-' + value + '">' +
			'		<span class="checkmark ' + value + '"></span>' +
			'	</label>' +
			'</div>'
		)
	});

	// 動態產出接頭選項
	$.each(headsetconnectorTitleArray, function(index, value) {
		$('.headsetconnector__group.items').append(
			'<div class="headsetconnector__item">' +
			'	<div class="form__group-radio">' +
			'		<label for="' + headsetconnectorCodeArray[index] + '">' +
			'			<input type="radio" name="headsetconnector__radioitem" value="' + headsetconnectorCodeArray[index] + '" id="' + headsetconnectorCodeArray[index] + '">' +
			'			<img src="images/headsetconnector/smallimg_' + headsetconnectorCodeArray[index] + '.png">' +
			'			<span>' + value + '</span>' +
			'		</label>' +
			'	</div>' +
			'</div>'
		)
	});

	// 動態產出電纜選項
	$.each(headsetcableTitleArray, function(index, value) {
		$('.headsetcable__group.items').append(
			'<div class="headsetcable__item">' +
			'	<div class="form__group-radio">' +
			'		<label for="' + headsetcableCodeArray[index] + '">' +
			'			<input type="radio" name="headsetcable__radioitem" value="' + headsetcableCodeArray[index] + '" id="' + headsetcableCodeArray[index] + '">' +
			'			<img src="images/headsetcable/smallimg_' + headsetcableCodeArray[index] + '.png">' +
			'			<span>' + value + '</span>' +
			'		</label>' +
			'	</div>' +
			'</div>'
		)
	});
	
	// 選擇耳機材質
	$('input[name^="headset__category"').click(function(){
		headsetMaterial = $(this).val();
		if (headsetColorTitleArray.indexOf(headsetFullEarColor) > -1 ) { headsetImgChange('F'); }
		if (headsetColorTitleArray.indexOf(headsetRightEarColor) > -1 ) { headsetImgChange('R'); }
		if (headsetColorTitleArray.indexOf(headsetLeftEarColor) > -1 ) { headsetImgChange('L'); }
		orderPriceHandle();
	});

	// 全耳耳機換圖
	$('.headset__color-group.fullear .form__group-radio').click(function(){
		headsetFullEarColor = $(this).find('input').val().replace('fullear-','');
		headsetRightEarColor = '';
		headsetLeftEarColor = '';		
		$('.headset__color-group.fullear .headset__color-selecteditem').removeClass().addClass('headset__color-selecteditem').addClass(headsetFullEarColor);
		$('.headset__color-group.fullear .headset__color-selectedtitle').text(headsetFullEarColor);
		headsetImgChange('F');
		orderPriceHandle();
	});

	// 右耳耳機換圖
	$('.headset__color-group.rightear .form__group-radio').click(function(){
		headsetFullEarColor = '';
		headsetRightEarColor = $(this).find('input').val().replace('rightear-','');
		$('.headset__color-group.rightear .headset__color-selecteditem').removeClass().addClass('headset__color-selecteditem').addClass(headsetRightEarColor);
		$('.headset__color-group.rightear .headset__color-selectedtitle').text(headsetRightEarColor);
		headsetImgChange('R');
		orderPriceHandle();
	});

	// 左耳耳機換圖
	$('.headset__color-group.leftear .form__group-radio').click(function(){
		headsetFullEarColor = '';
		headsetLeftEarColor = $(this).find('input').val().replace('leftear-','');
		$('.headset__color-group.leftear .headset__color-selecteditem').removeClass().addClass('headset__color-selecteditem').addClass(headsetLeftEarColor);
		$('.headset__color-group.leftear .headset__color-selectedtitle').text(headsetLeftEarColor);
		headsetImgChange('L');
		orderPriceHandle();
	});

	// 分別配置左右兩側 function
	$('#leftright').click(function(){
		if ($(this).is(':checked')){
			$('.headset__color-group.fullear').hide();
	  		$('.headset__color-group.rightear').show();
	  		$('.headset__color-group.leftear').show();
	  		headsetImgChange('L');
	  		headsetImgChange('R');
		} else {
			$('.headset__color-group.fullear').show();
			$('.headset__color-group.rightear').hide();
	  		$('.headset__color-group.leftear').hide();
	  		headsetImgChange('F');
		}
	});

	// 刪除UELOGO function
	$('#nouelogo').click(function(){
		nouelogo = !nouelogo;
		if (headsetColorTitleArray.indexOf(headsetFullEarColor) > -1 ) { headsetImgChange('F'); }
		if (headsetColorTitleArray.indexOf(headsetRightEarColor) > -1 ) { headsetImgChange('R'); }
		if (headsetColorTitleArray.indexOf(headsetLeftEarColor) > -1 ) { headsetImgChange('L'); }
	});

	$('#custompicture').click(function(){
		if($(this).is(':checked')) {
			$('.custompicture__lightbox.lightbox').show();
			custompicture = '是';
		} else {
			custompicture = '否';
		}
		if ($('.ordersummary__item-detail.custompicture .detail__content').length > 0) {
			$('.ordersummary__item-detail.custompicture .detail__content').html(custompicture);
		}
	});

	$('#custommirrorright').click(function(){
		console.log('that');
		if($(this).is(':checked')) {
			custommirrorright = '是';
		} else {
			custommirrorright = '否';
		}
		if ($('.ordersummary__item-detail.custommirrorright .detail__content').length > 0) {
			$('.ordersummary__item-detail.custommirrorright .detail__content').html(custommirrorright);
		}
	});

	// 處理耳機圖片
	function headsetImgChange(earfield){
		if (((headsetMaterialCodeArray.indexOf(headsetMaterial) > -1)) && (headsetColorTitleArray.indexOf(headsetFullEarColor) > -1 || (headsetColorTitleArray.indexOf(headsetRightEarColor) > -1 || headsetColorTitleArray.indexOf(headsetLeftEarColor) > -1))){
			// console.log(headsetMaterial, headsetFullEarColor, headsetRightEarColor, headsetLeftEarColor);
			const earImgPath = 'images/headset/';
			const nouelogoImgName = nouelogo === false ? '' : '-nouelogo';
			let rightearImgName = 'Rear.png';
			let leftearImgName = 'Lear.png';
			if (earfield === 'F'){
				if (typeof headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetFullEarColor)] !== 'undefined'){
					rightearImgName = headsetMaterial + '-' + headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetFullEarColor)] + '-R' + nouelogoImgName + '.png';
					leftearImgName = headsetMaterial + '-' + headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetFullEarColor)] + '-L' + nouelogoImgName + '.png';	
				}
				$('.headset__images-item.right img').attr('src', earImgPath + rightearImgName);
				$('.headset__images-item.left img').attr('src', earImgPath + leftearImgName);			
			} else if(earfield === 'R'){
				if (typeof headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetRightEarColor)] !== 'undefined'){
					rightearImgName = headsetMaterial + '-' + headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetRightEarColor)] + '-R' + nouelogoImgName + '.png';
					leftearImgName = headsetMaterial + '-' + headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetLeftEarColor)] + '-L' + nouelogoImgName + '.png';		
				}
				$('.headset__images-item.right img').attr('src', earImgPath + rightearImgName);
			} else if(earfield === 'L'){
				if (typeof headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetLeftEarColor)] !== 'undefined'){
					rightearImgName = headsetMaterial + '-' + headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetRightEarColor)] + '-R' + nouelogoImgName + '.png';
					leftearImgName = headsetMaterial + '-' + headsetColorCodeArray[headsetColorTitleArray.indexOf(headsetLeftEarColor)] + '-L' + nouelogoImgName + '.png';
				}
				$('.headset__images-item.left img').attr('src', earImgPath + leftearImgName);
			} else {
				$('.headset__images-item.right img').attr('src', earImgPath + rightearImgName);
				$('.headset__images-item.left img').attr('src', earImgPath + leftearImgName);
			}

			let ordersummary__headsetRightColor = headsetRightEarColor;
			let ordersummary__headsetLeftColor = headsetLeftEarColor;
			if (earfield === 'F') {
				ordersummary__headsetRightColor = headsetFullEarColor;
				ordersummary__headsetLeftColor = headsetFullEarColor;
			}

			let ordersummary__headsetmaterial = "<div class=\"ordersummary__item headsetmaterial\">" +
				"<div class=\"ordersummary__item-img\">" +
					"<img src=\"" + earImgPath + rightearImgName + "\">" +
					"<img src=\"" + earImgPath + leftearImgName + "\">" +
				"</div>" +
				"<div class=\"ordersummary__list\">" +
					"<div class=\"ordersummary__list-item\">" +
						"<div class=\"ordersummary__item-detail\">" +
							"<p class=\"detail__title\">模型</p>" +
							"<p class=\"detail__content\">最終耳型</p>" +
						"</div>" +
						"<div class=\"ordersummary__item-detail\">" +
							"<p class=\"detail__title\">本體</p>" +
							"<p class=\"detail__content\">清透</p>" +
						"</div>" +
					"</div>" +
					"<div class=\"ordersummary__list-item\">" +
						"<div class=\"ordersummary__item-detail\">" +
							"<p class=\"detail__title\">左面版</p>" +
							"<p class=\"detail__content\">" + ordersummary__headsetLeftColor + "</p>" +
						"</div>" +
						"<div class=\"ordersummary__item-detail\">" +
							"<p class=\"detail__title\">右面板</p>" +
							"<p class=\"detail__content\">" + ordersummary__headsetRightColor + "</p>" +
						"</div>" +
					"</div>" +
					"<div class=\"ordersummary__list-item \">" +
						"<div class=\"ordersummary__item-detail custompicture\">" +
							"<p class=\"detail__title\">上傳您的圖檔</p>" +
							"<p class=\"detail__content\">" + customleftright + "</p>" +
						"</div>" +
						"<div class=\"ordersummary__item-detail custommirrorright\">" +
							"<p class=\"detail__title\">向右鏡像</p>" +
							"<p class=\"detail__content\">" + custommirrorright + "</p>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"<div class=\"ordersummary__item-button\">" +
					"<button class=\"btn btn-edit\" data-scrolltarget=\"headsetmaterial\"><i class=\"fas fa-edit\"></i>　編輯</button>" +
				"</div>" +
			"</div>";

			$('#ordersummary__headsetmaterial').empty();
			$('#ordersummary__headsetmaterial').append(ordersummary__headsetmaterial);

			let orderconfirm__headsetImg = "<img src=\"" + earImgPath + rightearImgName + "\">" + 
										   "<img src=\"" + earImgPath + leftearImgName + "\">";

			$('.order__confirm-row.list .order__confirm-item.title').empty();							 
			$('.order__confirm-row.list .order__confirm-item.title').append(orderconfirm__headsetImg);
		}
	}
	
	// 處理耳機接頭換圖
	$('input[name="headsetconnector__radioitem"').click(function(){
		//console.log($(this).val());
		const fullimgPathandName = 'images/headsetconnector/fullimg_' + $(this).val() + '.png';
		$('.headsetconnector__fullimg img').attr('src', fullimgPathandName);
		const headsetconnector_title = $(this).parent().find('span').text();

		let ordersummary__headsetconnector = "<div class=\"ordersummary__item headsetconnector\">"+
		"<div class=\"ordersummary__item-img\">" +
			"<img src=\"" + fullimgPathandName + "\">" +
		"</div>" +
		"<div class=\"ordersummary__list\">" +
			"<div class=\"ordersummary__list-item\">" +
				"<div class=\"ordersummary__item-detail\">" +
					"<p class=\"detail__title\">接頭</p>" +
					"<p class=\"detail__content\">" + headsetconnector_title + "</p>" +
				"</div>" +
			"</div>" +
		"</div>" +
		"<div class=\"ordersummary__item-button\">" +
			"<button class=\"btn btn-edit\" data-scrolltarget=\"headsetconnector\"><i class=\"fas fa-edit\"></i>　編輯</button>" +
		"</div>" +
		"</div>";

		$('#ordersummary__headsetconnector').empty();
		$('#ordersummary__headsetconnector').append(ordersummary__headsetconnector);
		orderPriceHandle();
	});

	// 處理耳機電纜線換圖
	$('input[name="headsetcable__radioitem"').click(function(){
		//console.log($(this).val());
		const fullimgPathandName = 'images/headsetcable/fullimg_' + $(this).val() + '.png';
		$('.headsetcable__fullimg img').attr('src', fullimgPathandName);
		const headsetcable_title = $(this).parent().find('span').text();

		let ordersummary__headsetcable = "<div class=\"ordersummary__item headsetcable\">" +
			"<div class=\"ordersummary__item-img\">" +
				"<img src=\"" + fullimgPathandName + "\">" +
			"</div>" +
			"<div class=\"ordersummary__list\">" +
				"<div class=\"ordersummary__list-item\">" +
					"<div class=\"ordersummary__item-detail\">" +
						"<p class=\"detail__title\">電纜</p>" +
						"<p class=\"detail__content\">" + headsetcable_title + "</p>" +
					"</div>" +
				"</div>" +
			"</div>" +
			"<div class=\"ordersummary__item-button\">" +
				"<button class=\"btn btn-edit\" data-scrolltarget=\"headsetcable\"><i class=\"fas fa-edit\"></i>　編輯</button>" +
			"</div>" +
		"</div>";

		$('#ordersummary__headsetcable').empty();
		$('#ordersummary__headsetcable').append(ordersummary__headsetcable);
		orderPriceHandle();
	});

	// lightbox關閉視窗
	$('.custompicture__lightbox-content .btn-close').click(function(e){
		e.preventDefault();
		$('.lightbox').hide();
	});

	// 摘要編輯按鈕
	$(document).on("click", ".btn-edit" , function(e) {
		e.preventDefault();
		const scrollTarget = $(this).data('scrolltarget');
		// console.log(scrollTarget);
		const scrollTargetPositionY = $('.' + scrollTarget).offset().top;
		$("html, body").animate({ scrollTop: scrollTargetPositionY }, 600);
	});

	// 保存您的自訂義
	$('.btn-saveear').click(function(e){
		e.preventDefault();
		let LearImgPath = $('.headset__images-item.left img').attr('src');
		let RearImgPath = $('.headset__images-item.right img').attr('src');
		if (LearImgPath.indexOf('Lear.png') === -1) { window.open(LearImgPath, '_blank'); }
		if (RearImgPath.indexOf('Rear.png') === -1) { window.open(RearImgPath, '_blank'); }
	});

	// 分享到FB
	$('.btn-shareFB').click(function(e){
		e.preventDefault();
	});

	// 確認購物車編輯按鈕
	$(document).on("click", ".btn-submit" , function(e) {
		e.preventDefault();
		// 材料
		let material = $("input[name='headset__category']:checked").val() || '未填寫';
		// 全耳顏色
		let fullcolor = $("input[name='headset__color-fullear']:checked").val() || '未填寫';
		fullcolor = fullcolor.replace('fullear-','');
		// 是否有勾選左右耳
		let leftrightChecked = $("input[name='leftright']:checked").val() || '未選擇';
		// 右耳顏色
		let leftcolor = $("input[name='headset__color-leftear']:checked").val() || '未填寫';
		leftcolor = leftcolor.replace('leftear-','');
		// 左耳顏色
		let rightcolor = $("input[name='headset__color-rightear']:checked").val() || '未填寫';
		rightcolor = rightcolor.replace('rightear-','');
		// 刪除UELOGO
		let nouelogo = $('#nouelogo').is(':checked') === true ? '是' : '否';
		// 上傳你的藝術作品
		custompicture = $('#custompicture').is(':checked') === true ? '是' : '否';
		// 上傳你的藝術作品-分別配置左右兩側
		customleftright = $('#customleftright').is(':checked') === true ? '是' : '否';
		// 上傳你的藝術作品-向右鏡像
		custommirrorright = $('#custommirrorright').is(':checked') === true ? '是' : '否';
		// 接頭
		let connector = $("input[name='headsetconnector__radioitem']:checked").val() || '未填寫';
		// 電纜線
		let cable = $("input[name='headsetcable__radioitem']:checked").val() || '未填寫';
		// 訂單-對象
		let orderuser = $("input[name='orderuser']:checked").val() || '未填寫';
		// 訂單-名
		let lastname = $("input[name='lastname']").val() || '未填寫';
		// 訂單-姓
		let firstname = $("input[name='firstname']").val() || '未填寫';
		// 訂單-行動電話
		let ordermobile = $("input[name='ordermobile']").val() || '未填寫';
		// 訂單-連絡地址
		let orderaddress = $("input[name='orderaddress']").val() || '未填寫';
		// 訂單-電子郵件
		let email = $("input[name='email']").val() || '未填寫';
		let confirmemail = $("input[name='confirmemail']").val() || '未填寫';
		// 訂單-耳掃描
		let registered = $('#registered').is(':checked') === true ? '是' : '否';
		// 訂單-還沒有
		let newuser = $('#newuser').is(':checked') === true ? '是' : '否';
		// 訂單-STL文件
		let agent = $('#agent').is(':checked') === true ? '是' : '否';

		let errorMsgArray = [];

		if (material === '未填寫') { errorMsgArray.push('您尚未選擇耳機材料')};
		if (leftrightChecked === 'leftright') {
			if (leftcolor === '未填寫') { errorMsgArray.push('您尚未選擇左耳耳機材料')};
			if (rightcolor === '未填寫') { errorMsgArray.push('您尚未選擇右耳耳機材料')};	
		} else {
			if (fullcolor === '未填寫') { errorMsgArray.push('您尚未選擇全耳耳機顏色')};	
		}
		if (connector === '未填寫') { errorMsgArray.push('您尚未選擇接頭規格')};
		if (cable === '未填寫') { errorMsgArray.push('您尚未選擇電纜線規格')};
		if (orderuser === '未填寫') { errorMsgArray.push('您尚未填寫訂單對象')};
		if (lastname === '未填寫') { errorMsgArray.push('您尚未填寫訂單對象名')};
		if (firstname === '未填寫') { errorMsgArray.push('您尚未填寫訂單對象姓')};
		if (ordermobile === '未填寫') { errorMsgArray.push('您尚未填寫訂單行動電話')};
		if (orderaddress === '未填寫') { errorMsgArray.push('您尚未填寫訂單聯絡地址')};
		if (email === '未填寫') { errorMsgArray.push('您尚未填寫訂單資料Email')};
		if (email !== confirmemail) { errorMsgArray.push('訂單資料Email不一致')};

		if (errorMsgArray.length > 0) {
			let errMsg = errorMsgArray.join('\n');
			alert(errMsg);
		} else {
			$('.order__confirm').css('display', 'block');
			$('#order__confirm--producttitle').html(productTitle);
			$('#order__confirm--firstname').html(firstname);
			$('#order__confirm--lastname').html(lastname);
			$('#order__confirm--email').html(email);
			$('#order__confirm--ordermobile').html(ordermobile);
			$('#order__confirm--orderaddress').html(orderaddress);
			$('#order__confirm--registered').html(registered);
			if (leftrightChecked === 'leftright') {
				$('#order__confirm--leftcolor').html(leftcolor);
				$('#order__confirm--rightcolor').html(rightcolor);
			} else {
				$('#order__confirm--leftcolor').html(fullcolor);
				$('#order__confirm--rightcolor').html(fullcolor);
			}
			$('#order__confirm--nouelogo').html(nouelogo);
			$('#order__confirm--customleftright').html(customleftright);
			$('#order__confirm--connector').html(connector);
			$('#order__confirm--cable').html(cable);

			// 產品名稱 entry.1080769540
			// 材料 entry.1702282587
			// 全耳顏色 entry.1960166551
			// 右耳顏色 entry.178919103
			// 左耳顏色 entry.791237625
			// 刪除UELOGO entry.1087961856
			// 上傳你的藝術作品 entry.1649756552
			// 上傳你的藝術作品-分別配置左右兩側 entry.932401674
			// 上傳你的藝術作品-向右鏡像 entry.160138592
			// 接頭 entry.585956563
			// 電纜線 entry.1016356538
			// 訂單-對象 entry.2054362110
			// 訂單-名 entry.772152443
			// 訂單-姓 entry.886876278
			// 訂單-行動電話 entry.372211967
			// 訂單-聯絡地址 entry.105058540
			// 訂單-電子郵件 entry.1171605310
			// 訂單-耳掃描 entry.284668946
			// 訂單-現有客戶 entry.29335210
			// 訂單-STL文件 entry.1712319650

			orderdata = {
				'entry.1080769540': productTitle,
				'entry.1702282587': material,
				'entry.1960166551': fullcolor,
				'entry.178919103': leftcolor,
				'entry.791237625': rightcolor,
				'entry.1087961856': nouelogo,
				'entry.1649756552': custompicture,
				'entry.932401674': customleftright,
				'entry.160138592': custommirrorright,
				'entry.585956563': connector,
				'entry.1016356538': cable,
				'entry.2054362110': orderuser,
				'entry.772152443': lastname,
				'entry.886876278': firstname,
				'entry.372211967': ordermobile,
				'entry.105058540': orderaddress,
				'entry.1171605310': email,
				'entry.284668946': registered,
				'entry.29335210': newuser,
				'entry.1712319650': agent
			};
			orderPriceHandle();
		}
	});


	$('.order__confirm .btn-close').click(function(e){
		e.preventDefault();
		$('.order__confirm').hide();
	});

	// 送資料到Google表單
	// https://docs.google.com/forms/u/1/d/e/1FAIpQLSd796Ax3YdIoCQNZOUBkAvpJMORuNB9u2qZtYYEYiZ4VMKYzg/formResponse
	$('.btn-confirm').click(function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'https://docs.google.com/forms/u/1/d/e/1FAIpQLSd796Ax3YdIoCQNZOUBkAvpJMORuNB9u2qZtYYEYiZ4VMKYzg/formResponse',
			data: orderdata,
			contentType: 'application/json',
			dataType: 'jsonp',
			complete: function() {
				alert('資料已送出！');
				location.reload();
			}
		});
	});

	function orderPriceHandle(){
		// 全耳顏色
		let fullcolor = $("input[name='headset__color-fullear']:checked").val() || '未填寫';
		fullcolor = fullcolor.replace('fullear-','');
		// 是否有勾選左右耳
		let leftrightChecked = $("input[name='leftright']:checked").val() || '未選擇';
		// 右耳顏色
		let leftcolor = $("input[name='headset__color-leftear']:checked").val() || '未填寫';
		leftcolor = leftcolor.replace('leftear-','');
		// 左耳顏色
		let rightcolor = $("input[name='headset__color-rightear']:checked").val() || '未填寫';
		rightcolor = rightcolor.replace('rightear-','');

		let headsetFullearPrice = 0;
		let headsetRightearPrice = 0;
		let headsetLeftearPrice = 0;
		let headsetconnectorPrice = 0;
		let headsetcablePrice = 0;

		// 處理耳機價錢
		if (leftrightChecked === 'leftright') {
			headsetFullearPrice = 0;
			let headsetRightearSelectedItem = headsetList[headsetColorTitleArray.indexOf(rightcolor)];
			headsetRightearPrice = typeof headsetRightearSelectedItem !== 'undefined' ? headsetRightearSelectedItem['price'] : 0;

			let headsetLeftearSelectedItem = headsetList[headsetColorTitleArray.indexOf(leftcolor)];
			headsetLeftearPrice = typeof headsetLeftearSelectedItem !== 'undefined' ? headsetLeftearSelectedItem['price'] : 0;

		} else {
			let headsetSelectedItem = headsetList[headsetColorTitleArray.indexOf(fullcolor)];
			headsetFullearPrice = typeof headsetSelectedItem !== 'undefined' ? headsetSelectedItem['price'] : 0;
		}

		// 處理接頭價錢
		let connector = $("input[name='headsetconnector__radioitem']:checked").val() || '未填寫';
		let headsetconnectorSelectedItem = headsetconnectorList[headsetconnectorCodeArray.indexOf(connector)];
		headsetconnectorPrice = typeof headsetconnectorSelectedItem !== 'undefined' ? headsetconnectorSelectedItem['price'] : 0;

		// 處理電纜價錢
		let cable = $("input[name='headsetcable__radioitem']:checked").val() || '未填寫';
		let headsetcableSelectedItem = headsetcableList[headsetcableCodeArray.indexOf(cable)];
		headsetcablePrice = typeof headsetcableSelectedItem !== 'undefined' ? headsetcableSelectedItem['price'] : 0;

		orderprice = headsetFullearPrice * 2 + headsetRightearPrice + headsetLeftearPrice + headsetconnectorPrice + headsetcablePrice;
		$('.orderprice').html('$ ' + numberWithCommas(orderprice));
	}

	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
});