// 耳機材質 (材質名稱materialTitle / 材質代碼materialCode)
const headsetMaterialList = [
	{
		'materialTitle': '半透明',
		'materialCode': 'T'
	},
	{
		'materialTitle': '固體',
		'materialCode': 'S'
	}, 
	{
		'materialTitle': '火花',
		'materialCode': 'F'
	},
	{
		'materialTitle': '專業',
		'materialCode': 'P'
	}
];

// 耳機顏色 (顏色名稱colorTitle / 顏色圖檔代碼colorCode / 耳機價錢price)
const headsetList = [
	{
		'colorTitle': 'pinky',
		'colorCode': 'P',
		'price': 15000
	},
	{
		'colorTitle': 'red',
		'colorCode': 'R',
		'price': 15000
	},
	{
		'colorTitle': 'blue',
		'colorCode': 'B',
		'price': 15000
	},
	{
		'colorTitle': 'white',
		'colorCode': 'W',
		'price': 15000
	},
	{
		'colorTitle': 'lightgary',
		'colorCode': 'LG',
		'price': 15000
	},
	{
		'colorTitle': 'darkgray',
		'colorCode': 'DG',
		'price': 15000
	}
];

// 接頭 (接頭名稱connectorTitle / 接頭代碼connectorCode / 接頭價錢price)
let headsetconnectorList = [
	{
		'connectorTitle': 'IME2',
		'connectorCode': 'IME2',
		'price': 5000
	},
	{
		'connectorTitle': 'MMCX',
		'connectorCode': 'MMCX',
		'price': 5000
	},
];

// 電纜 (電纜名稱cableTitle / 電纜代碼cableCode / 電纜價錢price)
let headsetcableList = [
	{
		'cableTitle': '50英寸IPX Earloop黑色電纜',
		'cableCode': 'IPX50',
		'price': 5000
	},
	{
		'cableTitle': '64" IPX透明電纜',
		'cableCode': 'IPX64',
		'price': 5000
	},
];